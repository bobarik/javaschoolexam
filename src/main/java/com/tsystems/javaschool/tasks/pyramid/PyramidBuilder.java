package com.tsystems.javaschool.tasks.pyramid;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        /*
        Number of elements should be equal to the sum of arithmetic progression x(n+1) = (x(n) + 1)
        Sum of arithmetic progression(up to nth element) can be found using this formula: sum(n) = (x(1) + x(n)) / 2 * n
        sum(n) = (1 + n) / 2 * n = (n + n ^ 2) / 2
        n here would be a number of rows in the pyramid
        To find it we need to solve quadratic equation: n^2/2 + n/2 - c = 0, where c is inputNumbers.size()
        as n should always be positive, we only need the positive solution for the equation.
        det = 0.25 + 2 * c
        n = (sqrt(det) - 0.5) / (2 * 0.5) = sqrt(det) - 0.5
        n must be an integer to be able to create the Pyramid.
        number of columns in the pyramid is equal to n * 2 - 1
        */

        double c = inputNumbers.size();

        BigDecimal det = new BigDecimal(0.25 + 2 * c);
        BigDecimal n = det.sqrt(MathContext.DECIMAL128); //Switch to Java 9 to use BigDecimal.sqrt()
        n = n.subtract(new BigDecimal("0.5"));

        //Can't put null in the array of primitives.
        if (inputNumbers.stream().anyMatch(Objects::isNull)) {
            throw new CannotBuildPyramidException();
        }

        //Checking if n is an integer
        if (n.stripTrailingZeros().scale() <= 0) {
            int rows = n.intValue();
            int columns = rows * 2 - 1;
            int[][] pyramid = new int[rows][columns];

            //sorting inputNumbers
            Iterator<Integer> j = inputNumbers.stream().sorted(Integer::compareTo).iterator();

            for (int i = 0; i < rows; i++) {
                int k = rows - 1 - i;
                for (int l = 0; l < i + 1; k += 2, l++) {
                    pyramid[i][k] = j.next();
                }
            }
            return pyramid;
        } else {
            throw new CannotBuildPyramidException();
        }
    }
}
