package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        //Checking if either of Lists is null to avoid NullPointerException
        if(x == null || y == null) {
            throw new IllegalArgumentException("Neither of Lists can't be null");
        }

        //Iterating x using listIterator while also iteration y using forEach.
        ListIterator i = x.listIterator();
        for (Object j :
                y) {
            if (!i.hasNext()) return true;
            if (j.equals(x.get(i.nextIndex()))) {
                i.next();
            }
        }

        //If we didn't complete our iteration over x, then we can't get a sequence equal to it from y.
        return !i.hasNext();
    }
}
