package com.tsystems.javaschool.tasks.calculator;

import java.util.function.BinaryOperator;

public enum Operator {
    ADD('+', 1),
    SUB('-', 1),
    MUL('*', 2),
    DIV('/', 2),
    LPAR('(', 4),
    RPAR(')', 4);

    public final char sign;
    public final int precedence;
    Operator(char sign, int precedence) {
        this.sign = sign;
        this.precedence = precedence;
    }

    public String toString() {
        return String.valueOf(sign);
    }

    public static Operator forSymbol(char symbol) {
        for (Operator operator : values()) {
            if (operator.sign == symbol) {
                return operator;
            }
        }
        return null;
    }
}
