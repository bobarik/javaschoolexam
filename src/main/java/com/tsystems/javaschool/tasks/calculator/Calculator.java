package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        //There are many ways to write an infix calculator, I chose to use shunting yard (Sorting station in Russian) algorithm
        // to parse infix expression to produce postfix notation string and use it to solve the expression.
        if (statement == null) return null;

        //Removing unnecessary characters like whitespace
        statement = statement.replaceAll(" ", "");

        statement = shuntingYard(statement);

        //Formatting Double: '.' instead of ',', round to 4 significant digits.
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
        DecimalFormat format = new DecimalFormat("#.####", symbols);

        String res = parseRPN(statement);
        if (res == null) return null;

        Double result = Double.valueOf(res);

        return format.format(result);
    }

    public String shuntingYard(String statement) {
        Stack<Operator> operators = new Stack<>();
        StringBuilder RPNStatement = new StringBuilder();

        int i = 0;
        while (i < statement.length()) {
            char c = statement.charAt(i++);
            boolean wasNumber = false;
            int numOfDec = 0; //Checking for 2 or more decimal separators in 1 number.

            //If we find a digit, we're starting to parse the whole number.
            while ((Character.isDigit(c) || c == '.') && i <= statement.length()) {
                wasNumber = true;
                if (c == '.') numOfDec++;
                if (numOfDec > 1) return null;
                RPNStatement.append(c);
                if (i < statement.length()) {
                    c = statement.charAt(i);
                }
                i++;
            }

            //Append " " to differentiate between different numbers.
            if (wasNumber) {
                RPNStatement.append(" ");
            }

            Operator operator = Operator.forSymbol(c);
            if (operator != null) {
                //If it is an operator, check operator stack.
                switch (operator) {
                    //If it's the Right Bracket(Parenthesis), append every operator until Left Bracket, then remove it.
                    case RPAR:
                        while (!operators.isEmpty() && operators.peek() != Operator.LPAR) {
                            RPNStatement.append(operators.pop().toString()).append(" ");
                        }
                        if (operators.isEmpty()) return null;
                        operators.pop();
                        break;
                    // If it isn't Left bracket, check for precedence and append every operator with precedence higher or equal to its own.
                    default:
                        while (!operators.isEmpty()
                                && operator.precedence <= operators.peek().precedence
                                && operators.peek() != Operator.LPAR) {
                            RPNStatement.append(operators.pop().toString()).append(" ");
                        }
                    //If it's Left bracket (or at the end of any other operator's action, excluding Right bracket), push into the stack.
                    case LPAR:
                        operators.push(operator);
                }
            } else {
                //If it's neither an operation nor a digit, return null
                if (!Character.isDigit(c)) return null;
            }
        }

        //Append remaining operators into the postfix notation.
        while (!operators.isEmpty()) {
            if (operators.peek() == Operator.LPAR) return null;
            RPNStatement.append(operators.pop().toString()).append(" ");
        }

        return RPNStatement.toString();
    }

    //Parsing Reverse Polish Notation
    public String parseRPN(String x) {
        if (x == null || x.isEmpty())
            return null;

        //Getting tokens from string
        String[] tokens = x.split(" ");

        Stack<String> operands = new Stack<>();

        for (String s :
                tokens) {
            //If our token is a number, push it in the stack.
            if (s.matches("[0-9]+.?[0-9]*")) {
                operands.push(s);
            } else {
                //If our token is operation, check if stack is empty, then perform operation on 2 most recent elements.
                if (operands.isEmpty()) return null;
                Double right = Double.valueOf(operands.pop());

                if (operands.isEmpty()) return null;
                Double left = Double.valueOf(operands.pop());

                Operator operator = Operator.forSymbol(s.charAt(0));
                if (operator == null) return null;
                switch (operator) {
                    case ADD:
                        operands.push(Double.toString(left + right));
                        break;
                    case SUB:
                        operands.push(Double.toString(left - right));
                        break;
                    case MUL:
                        operands.push(Double.toString(left * right));
                        break;
                    case DIV:
                        //Returning null instead of INF
                        if (right == 0) return null;
                        operands.push(Double.toString(left / right));
                        break;
                    default:
                        return null;
                }
            }
        }

        return operands.pop();
    }
}
